---
title: Welcome
type: docs
---

# Welcome

---

## Links

{{< columns >}}
- <http://www.rader.us>
<--->
- [docs/node-red/](http://www.rader.us/docs/node-red/)
- [docs/rpi-config/](http://www.rader.us/docs/rpi-config/)
- [docs/symbolic-logic/](http://www.rader.us/docs/symbolic-logic/)
- [docs/scheme/](http://www.rader.us/docs/scheme/)
{{< /columns >}}

{{< columns >}}
- <https://kirkrader.gitlab.io>
<--->
- [source](https://gitlab.com/kirkrader/kirkrader.gitlab.io)
{{< /columns >}}

{{< columns >}}
- <https://parasaurolophus.gitlab.io>
<--->
- [source](https://gitlab.com/parasaurolophus/parasaurolophus.gitlab.io)
{{< /columns >}}

- <https://gitlab.com/kirkrader>
- <http://www.cdbaby.com/Artist/KirkRader>

---

{{< katex >}}
\begin{aligned}
  \text{Let } \Omega &= \omega \omega \\
\text{where } \omega &= \lambda x.x x
\end{aligned}
{{< /katex >}}

<hr>

<p>
  <a href="/time.php">time.php</a>
</p>

<hr>
