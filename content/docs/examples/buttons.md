---
title: "Buttons"
date: 2019-09-18T17:31:37-07:00
weight: 1
---

# Buttons

```tpl
{{</* button relref="/" [class="..."] */>}}Home{{</* /button */>}}
{{</* button href=""https://gitlab.com/kirkrader/hugo-project" */>}}Contribute{{</* /button */>}}
```

---

{{< button relref="/" >}}Home{{< /button >}}
{{< button href="https://gitlab.com/kirkrader/hugo-project" >}}Source{{< /button >}}
