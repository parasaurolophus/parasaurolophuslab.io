---
title: "Expand"
date: 2019-09-18T17:31:04-07:00
weight: 3
---

# Expand

## Default Label

```tpl
{{</* expand */>}}
### Expanded Content
Here is the content that is hidden until it is expanded.
{{</* /expand */>}}
```

---

{{< expand >}}
### Expanded Content
Here is the content that is hidden until it is expanded.
{{< /expand >}}

## Custom Label

```tpl
{{</* expand "Custom Label" "..." */>}}
### Expanded Content
Here is the content that is hidden until it is expanded.
{{</* /expand */>}}
```

---

{{< expand "Custom Label" "..." >}}
### Expanded Content
Here is the content that is hidden until it is expanded.
{{< /expand >}}
