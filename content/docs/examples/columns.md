---
title: "Columns"
date: 2019-09-18T18:45:39-07:00
weight: 2
---

# Columns

```tpl
{{</* columns */>}} <!-- begin columns block -->

# Left

Here is the content for the left column.

<---> <!-- magic sparator, between columns -->

# Middle

Here is the content for the middle column.

<---> <!-- magic sparator, between columns -->

# Right

Here is the content for the right column.

{{</* /columns */>}}
```

---

{{< columns >}}

## Left

Here is the content for the left column.

<--->

## Middle

Here is the content for the middle column.

<--->

## Right

Here is the content for the right column.

{{< /columns >}}
