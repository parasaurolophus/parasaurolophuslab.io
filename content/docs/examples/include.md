---
title: "External Content"
date: 2019-09-18T17:30:54-07:00
weight: 4
---

# External Content

## Include

```tpl
{{</* include file="/static/include.md" */>}}
```

---

{{< include file="/static/include.md" >}}

## Code

```tpl
{{</* code file="/static/include.scm" language="lisp" */>}}
```

---

{{< code file="/static/include.scm" language="lisp" >}}
