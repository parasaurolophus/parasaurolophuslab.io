---
title: "Tabs"
date: 2019-09-18T17:31:31-07:00
weight: 7
---

# Tabs

```tpl
{{</* tabs "uniqueid" */>}}
{{</* tab "Tab 1" */>}}
## Tab 1

Content for tab 1
{{</* /tab */>}}

{{</* tab "Tab 2" */>}}
## Tab 2

Content for tab 2
{{</* /tab */>}}

{{</* tab "Tab 3" */>}}
## Tab 3

Content for tab 3
{{</* /tab */>}}

{{</* /tabs */>}}
```

---

{{< tabs "uniqueid" >}}

{{< tab "Tab 1" >}}
## Tab 1

Content for tab 1
{{< /tab >}}

{{< tab "Tab 2" >}}

## Tab 2

Content for tab 2
{{< /tab >}}

{{< tab "Tab 3" >}}

## Tab 3

Content for tab 3
{{< /tab >}}

{{< /tabs >}}
