---
title: "Mermaid"
date: 2019-09-18T17:31:46-07:00
weight: 6
---

# Mermaid

```tpl
{{</* mermaid [class="text-center"]*/>}}
graph TB
    start(( ))
    inception[Inception / Business Analysis]
    execution[Execution / Development]
    verification[Verification / QA]
    operation[Operation / Tech Support]
    start-- request feature -->inception
    inception-- reject -->start
    inception-- analyze -->inception
    inception-- approve -->execution
    execution-- design / implement -->execution
    execution-- request change -->inception
    execution-- release -->verification
    verification-- report bug -->execution
    verification-- test -->verification
    verification-- request change -->inception
    verification-- accept -->operation
    operation-- monitor -->operation
    operation-- report bug -->execution
    operation-- request change -->inception
    operation-- anayltics -->inception
{{</* /mermaid */>}}
```

---

{{< mermaid >}}
graph TB
    start(( ))
    inception[Inception / Business Analysis]
    execution[Execution / Development]
    verification[Verification / QA]
    operation[Operation / Tech Support]
    start-- request feature -->inception
    inception-- reject -->start
    inception-- analyze -->inception
    inception-- approve -->execution
    execution-- design / implement -->execution
    execution-- request change -->inception
    execution-- release -->verification
    verification-- report bug -->execution
    verification-- test -->verification
    verification-- request change -->inception
    verification-- accept -->operation
    operation-- monitor -->operation
    operation-- report bug -->execution
    operation-- request change -->inception
    operation-- anayltics -->inception
{{< /mermaid >}}
