---
title: "KaTex"
date: 2019-09-18T17:31:54-07:00
weight: 5
---

# KaTex

```latex
{{</* katex */>}}
{ \lim_{n \to 0} { 1 \over n } } = \infty
{{</* /katex */>}}
```

---

{{< katex >}}
{ \lim_{n \to 0} { 1 \over n } } = \infty
{{< /katex >}}
