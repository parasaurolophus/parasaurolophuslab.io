---
title: "Shourtcode Examples"
weight: 1
# bookFlatSection: false
# bookShowToC: true
---

# Shortcode Examples

Example shortcode usage:

- [Buttons]({{< relref "buttons" >}})
- [Columns]({{< relref "columns" >}})
- [Expand]({{< relref "expand" >}})
- [Include]({{< relref "include" >}})
- [KaTeX]({{< relref "katex" >}})
- [Mermaid]({{< relref "mermaid" >}})
- [Tabs]({{< relref "tabs" >}})
